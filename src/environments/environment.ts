// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAK1qOUkA0_rBSaR65BYKgbVNHGiM9yLrg",
    authDomain: "again-ff938.firebaseapp.com",
    projectId: "again-ff938",
    storageBucket: "again-ff938.appspot.com",
    messagingSenderId: "170051943043",
    appId: "1:170051943043:web:c3658d346c16e723cc79b6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
