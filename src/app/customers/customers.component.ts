import { LambdaService } from './../lambda.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomerService } from '../customer.service';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId:string;
  customers$;
  customers:Customer[];
  hiddenForm:boolean = true;

  rowToEdit:number = -1; 
  customerToEdit:Customer = {name:null, education:null, income:null};
  wrongEducation = [];

  displayedColumns: string[] = ['name', 'education', 'income', 'delete', 'edit', 'predict', 'result'];

  add(customer:Customer){
    this.customersService.addCustomer(this.userId,customer.name,customer.education,customer.income);
  }

  delete(customerId:string){
    this.customersService.deleteCustomer(this.userId,customerId);
  }

  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.education = this.customers[index].education;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
    this.wrongEducation[index] = false;
  }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    if(this.customerToEdit.education<0 || this.customerToEdit.education>24){
      this.wrongEducation[this.rowToEdit] = true;
    }else{
      this.customersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.education,this.customerToEdit.income);
      this.wrongEducation[this.rowToEdit] = false;
      this.rowToEdit = null;
      
    }
  }

  sendData(index,education:number,income:number){
    console.log(income);
    this.lambdaService.predict(education,income).subscribe(
      res => {console.log(res);
        if (res > 0.5){
          var result = 'will pay';
        }else{
          var result = 'will not pay';
        }
    this.customers[index].result = result;
    }
    );
  }

    saveResult(index,customerId,result){
    this.customersService.updateResult(this.userId,customerId,result);
    this.customers[index].saved = true;
  }


  constructor(private customersService:CustomerService, public authService:AuthService, private lambdaService:LambdaService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              for (let document of docs) {
                const customer:Customer = document.payload.doc.data();
                if (customer.result){
                  customer.saved = true;
                }
                customer.id = document.payload.doc.id;
                this.customers.push(customer); 
              }                        
            }
          )
      })
 }
  }