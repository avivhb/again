import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  name:string;
  education:number;
  income:number;
  wrongEducation:boolean = false;

  @Output() update = new EventEmitter<Customer>();
  @Output() closeForm = new EventEmitter<null>();


  updateParent(){
    let customer:Customer = {name:this.name,education:this.education, income:this.income};
      if(this.education<0 || this.education>24){
        this.wrongEducation = true;
      }else{
      this.update.emit(customer);
      this.name = null;
      this.education = null;
      this.income = null;
    }
  }
  

  tellParentToClose(){
    this.closeForm.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
