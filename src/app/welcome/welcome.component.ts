import { LambdaService } from './../lambda.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  result;

  sendData(num1,num2){
    this.lambdaService.predict(num1,num2).subscribe(
      res => {console.log(res);
      this.result = res;
    }
    )
  }

  constructor(private lambdaService:LambdaService) { }

  ngOnInit(): void {
  }

}
